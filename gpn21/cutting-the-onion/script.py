i = 9 # Image
j = 7 # Label
k = 3 # Filename

while i <= 34:
	s = """\\begin{figure}
    \\centering
    \\includegraphics[scale=0.22]{img/or""" + str(i) + """.png}
    \\caption{Onion Routing}
    \\label{fig:or""" + str(j) + """}
\\end{figure}"""
	file = open("or/or" + str(k) + ".tex", "w+")
	file.write(s)
	file.close()
	i += 1
	j += 1
	k += 1

#i = 3
#while i <= 28:
#	s = """\\begin{frame}{Onion Routing}
#    \include{or/or""" + str(i) + """}
#\end{frame}
#"""
#	print(s)
#	i += 1
